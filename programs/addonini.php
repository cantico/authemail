; <?php/*
[general]
name							="AuthEmail"
version							="0.4.7"
encoding						="UTF-8"
mysql_character_set_database	="latin1,utf8"
description						="Authenticate by email instead of nickname"
description.fr					="Module d'authentification par email à la place de l'identifiant"
long_description.fr             ="README.md"
delete							=1
ov_version						="8.1.98"
php_version						="5.1.0"
addon_access_control			=0
author							="Cantico"
icon							="icon.png"
configuration_page 				="admin"
tags						    ="library,authentication"

;*/?>