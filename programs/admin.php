<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';


require_once dirname(__FILE__).'/functions.php';
require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';



class AuthEmail_ConfigurationPage
{


	private function getForm()
	{
		$W = bab_Widgets();
		$form = $W->Form();
		$form->setName('configuration')
			->addClass('BabLoginMenuBackground')
			->addClass('widget-bordered')
			->addClass('widget-centered');
		
		$form->setCanvasOptions($form->Options()->width(70,'em'));
		
		$form->setHiddenValue('tg', bab_rp('tg'));
		$form->colon();

		$form->getLayout()->setVerticalSpacing(1,'em');
		
		$form->addItem($W->Label(AuthEmail_translate('Reset nickname using email'))->addClass('widget-strong'));
		$form->addItem($W->Label(AuthEmail_translate('Warning, the nicknames for all users in will be lost, the email will be copied over the nickname field if possible')));
		
		$label = $W->Label(AuthEmail_translate('Process all users from group'));
		$input = $W->GroupPicker()->setAssociatedLabel($label)->setName('group');
		
		$form->addItem($W->VBoxItems($label, $input));
		
		
		$form->addItem(
				$W->SubmitButton()
				->setLabel(AuthEmail_translate('Reset nicknames'))
		);

		
		$form->setValues(
			array(
				'configuration' => array(
					'group'		=> BAB_REGISTERED_GROUP
				)
			)
		);

		return $form;
	}




	public function display()
	{
		$W = bab_Widgets();
		$page = $W->BabPage();
		

		$page->addItem($this->getForm());
		$page->displayHtml();
	}


	public function save($configuration)
	{
		global $babBody;
		/*@var $babBody babBody */
		$id_group = (int) $configuration['group'];
		
		if (!$id_group)
		{
			$babBody->addError(AuthEmail_translate('This group is not valid'));
			return false;
		}
		
		$arr = bab_getGroupsMembers($id_group, true, true);
		
		if (empty($arr))
		{
			$babBody->addError(AuthEmail_translate('This group is empty'));
			return false;
		}
		
		$count = 0;
		
		foreach($arr as $user)
		{
			$error = '';
			if (!bab_updateUserById($user['id'], array('nickname' => $user['email']), $error))
			{
				if ($error)
				{
					$babBody->addError($user['name'].' : '.$error);
					$babBody->addNextPageError($user['name'].' : '.$error);
				}
			} else {
				$count++;
			}
		}
		
		$babBody->addNextPageMessage(sprintf(AuthEmail_translate('Modification done on %d user accounts'), $count));
		bab_url::get_request('tg')->location();
	}
}


if (!bab_isUserAdministrator())
{
	return;
}


$page = new AuthEmail_ConfigurationPage;

if (!empty($_POST))
{
	$page->save(bab_pp('configuration'));
}

$page->display();
