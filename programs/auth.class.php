<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/functions.php';


bab_functionality::includeFile('PortalAuthentication');




class Func_PortalAuthentication_AuthEmail extends Func_PortalAuthentication
{
    

	public function getDescription() 
	{
		return AuthEmail_translate("Email Authentication");
	}
	
	/**
	 * If authentication use ovidentia database
	 * This is used by the emailpassword functionality and administrator users list
	 * @var string
	 */
	public function getLoginIdField() {
	    return 'email';
	}
	
	
	/**
	 * overwrite the default method
	 * 
	 * (non-PHPdoc)
	 * @see Func_PortalAuthentication::authenticateUser()
	 */
	public function authenticateUser($sLogin, $sPassword)
	{
		// authenticate on ovidentia database
		$AuthObject = bab_functionality::get('PortalAuthentication/AuthOvidentia');
		/*@var $AuthObject Func_PortalAuthentication_AuthOvidentia */
		
		$id_user = $AuthObject->authenticateUserByEmailPassword($sLogin, $sPassword);
		
		$this->errorMessages = $AuthObject->errorMessages;
		
		return $id_user;
	}

	
	
	public function login()
	{
		$email		= bab_pp('email', null);
		$sPassword	= bab_pp('password', null);
		$iLifeTime	= (int) bab_pp('lifetime', 0);

		if (!empty($email) && !empty($sPassword))
		{
			$iIdUser = $this->authenticateUser($email, $sPassword);

			if ($this->userCanLogin($iIdUser))
			{
				$this->setUserSession($iIdUser, $iLifeTime);
				return true;
			}
		}
		else if ($email === '' || $sPassword === '')
		{
			$this->addError(bab_translate("You must complete all fields !!"));
		}
		
		require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
		
		$addon = bab_getAddonInfosInstance('AuthEmail');
		
		if (false === $addon)
		{
			// si le module n'est pas installe on ne l'utilise pas
			
			$AuthObject = bab_functionality::get('PortalAuthentication/AuthOvidentia');
			/*@var $AuthObject Func_PortalAuthentication_AuthOvidentia */
			return $AuthObject->login();
		}
		
		$url = new bab_url($addon->getUrl().'login');
		$url->msg = $this->loginMessage;
		$url->err = implode("\n", $this->errorMessages);
		
		$url->location();
		exit;
	}
	
	
	
	
	
	
	public function logout()
	{
		
		$AuthObject = bab_functionality::get('PortalAuthentication/AuthOvidentia');
		return $AuthObject->logout();
	}
}






