<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
include 'base.php';
require_once dirname(__FILE__).'/functions.php';

bab_functionality::includeOriginal('UserEditor');



class Func_UserEditor_AuthEmail extends Func_UserEditor
{

	public function getDescription()
	{
		return AuthEmail_translate('User modification without nickname');
	}
	
	
	/**
	 * Test if the nickname of user can be modified
	 * do not overwrite the nickname field with email in user modification
	 * @param int $id_user
	 */
	protected function canEditNickname($id_user)
	{
		return false;
	}
	
	
	/**
	 * Disable nickname field
	 * 
	 */
	protected function getNicknameWidget($id_user)
	{
		return null;
	}
	
	/**
	 * Save from posted array
	 * and send the notifications if necessary
	 * return id_user
	 *
	 * @throws Exception
	 *
	 * @return int
	 */
	public function save(Array $user)
	{
		if($this->register){
			$duplicate = bab_getUserIdByEmail($user['email']);
			if($duplicate > 0){
				throw new Exception(sprintf(AuthEmail_translate('The email %s allready exists'), $user['email']));
			}
		}

		$user['nickname'] = $user['email'];
		
		return parent::save($user);
	}
}