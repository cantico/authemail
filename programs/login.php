<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/functions.php';
require_once $GLOBALS['babInstallPath'].'utilit/loginIncl.php';

class AuthEmail_displayLogin_Template
{
	var $nickname;
	var $password;

	public function __construct($url)
	{
		$this->email = bab_translate("Email");
		$this->password = bab_translate("Password");
		$this->login = bab_translate("Login");

		// verify and buid url
		$params = array();
		$arr = explode('?',$url);

		if (isset($arr[1])) {
			$params = explode('&',$arr[1]);
		}

		$url = $GLOBALS['babPhpSelf'];

		foreach($params as $key => $param) {
			$arr = explode('=',$param);
			if (2 == count($arr)) {

				$params[$key] = $arr[0].'='.$arr[1];
			} else {
				unset($params[$key]);
			}
		}

		if (0 < count($params)) {
			$url .= '?'.implode('&',$params);
		}

		$url = str_replace("\n",'', $url);
		$url = str_replace("\r",'', $url);
		$url = str_replace('%0d','', $url);
		$url = str_replace('%0a','', $url);

		$this->referer = bab_toHtml($url);
		$this->life = bab_translate("Connection timeout");
		$this->nolife = bab_translate("No");
		$this->oneday = bab_translate("one day");
		$this->oneweek = bab_translate("one week");
		$this->onemonth = bab_translate("one month");
		$this->oneyear = bab_translate("one year");
		$this->infinite = bab_translate("unlimited");

		$this->c_nickname = isset($_COOKIE['c_nickname']) ? bab_toHtml($_COOKIE['c_nickname']) : '';
	}
}



function AuthEmail_Error($msg)
{
	global $babBody;
	
	$babBody->addError($msg);
	$babBody->babEcho(sprintf('<a href="%s">%s</a>', bab_toHtml($GLOBALS['babUrlScript'].'?tg=login&cmd=signon&sAuthType=Ovidentia'), AuthEmail_translate('Continue')));
}



/**
 * This form allow email login but no registration of new account
 *
 */
function AuthEmail_displayLoginForm()
{
	global $babBody;

	$title = bab_rp('msg', '');
	$errorMessages = bab_rp('err', '');

	$babBody->setTitle($title);
	$errors = explode("\n", $errorMessages);
	foreach ($errors as $errorMessage) {
		$babBody->addError($errorMessage);
	}
	$babBody->addItemMenu('signon', bab_translate("Login"), '');
	
	$settings = bab_getInstance('bab_Settings');
	/*@var $settings bab_Settings */
	$site = $settings->getSiteSettings();
	
	if($site['registration'] == 'Y') {
		$babBody->addItemMenu('register', bab_translate("Register"), $GLOBALS['babUrlScript'].'?tg=login&cmd=register');
	}

	if(isEmailPassword())
	{
		$babBody->addItemMenu('emailpwd', bab_translate("Lost Password"), $GLOBALS['babUrlScript'].'?tg=login&cmd=emailpwd');
	}

	
	if (BAB_AUTHENTIFICATION_OVIDENTIA !== (int) $babBody->babsite['authentification'])
	{
		AuthEmail_Error(AuthEmail_translate('The authentication must be configured to use the ovidentia database while using the AuthEmail addon'));
		return false;
	}
	
	$babBody->setCurrentItemMenu('signon');

	$temp = new AuthEmail_displayLogin_Template(bab_rp('referer'));
	$addon = bab_getAddonInfosInstance('AuthEmail');
	
	$html = $addon->printTemplate($temp, 'login.html', 'login');
	
	if (function_exists('bab_displayLoginPage')) {
	    bab_displayLoginPage($html, 'signon.html');
	} else {
	    $babBody->babecho($html);
	}
	
	
}






AuthEmail_displayLoginForm();