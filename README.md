## Authentification par email ##

Permet de s'authentifier par email à la place de l'identifiant. Ce module contient actuellement deux limitations :

* Il est impossible d'activer l'inscription sur le site lorsque ce module est activé.
* Lors de la perte du mot de passe, le formulaire de récupération demande l'identifiant.